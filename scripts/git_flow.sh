#!/bin/bash

# Git flow helper
# Need git credentials!


NICE='\033[1;32m'
NC='\033[0m'
RED='\033[0;31m'

LC_ALL=C
LANGUAGE=en_US:en

PROJECT_V50_PATH=$(echo ${PWD} | grep -o '^.*unifw_icatch_v50')
DEVICE=""

LINE="0"
COMMIT=""
COMMIT_CNT="3"
HASH_LEN=40
ACTIVE_DEVICE=""
RELEASE_TYPE=""
REL_TYPES=( new hotfixed )
TOML_CONFIG_NAME="config.toml"

help()
{
cat << EOF
./git_flow <command>
    commands:
    sync         - create combine commit in the second level folder structure(unifw_icatch_v50).
                   call after merge your pr.
    release      - create release commit for the specific device in the first level folder structure (unifw_devices) 
    hotfix_begin - initialize work with the release hotfix.
    hotfix_end   - finalize hotfix work
EOF
}

finalize()
{
    cd ${PROJECT_V50_PATH}/..
    # Restore working commits for all repo (exept PROJECT_V50_PATH, because we already create sync commit here)
    printf "${NICE} Step final: Restore working commits for all repo ...  ${NC}\n"

    if [[ -f submodules_all_info ]]; then

        while read  line
        do
            path=$(echo ${line#+*} | awk '{ print $2 }')
            commit=$(echo ${line#+*} | awk '{ print $1 }')

            if [[ ${path} == "unifw_icatch_v50" ]]; then
                continue;
            fi

            cd ${path}
            git checkout $(git name-rev ${commit} --name-only ) -q

            cd -  > /dev/null
            printf "${path} ${NICE} restored ${NC}\n"
        done < submodules_all_info

        rm submodules_all_info
    fi

    rm -f submodules_info
    rm -f RC_MESSAGE
}

script_terminate()
{
    echo
    finalize
    exit 0
}
trap script_terminate INT TERM


if [[ ${PROJECT_V50_PATH} == "" ]]; then
    echo "Invalid project path! Please, run this script under 'unifw_icatch_v50' folder "
    exit 1
fi

select_rc()
{
    cmt_cnt=$(git rev-list --count HEAD)

    read -n 1 -s -p $'\n --------\n 1 - Next\n 2 - Prev\n 3 - Select\n other - Exit\n --------\n' pressed
    case $pressed in
        "1")
            LINE=$((LINE+${COMMIT_CNT}))
            if (( ${LINE} > ${cmt_cnt} )); then
                LINE=${cmt_cnt}
            fi

            if [[ ${RELEASE_TYPE} == "hotfixed" ]]; then
                git log develop -${LINE} --skip $((LINE-${COMMIT_CNT})) --max-count=${COMMIT_CNT} --grep="(hotfix)"
            else
                git log develop -${LINE} --skip $((LINE-${COMMIT_CNT})) --max-count=${COMMIT_CNT} --grep="(hotfix)" --invert-grep
            fi

            return 0
            ;;
        "2")
            LINE=$((LINE-${COMMIT_CNT}))
            if (( ${LINE} < 0 )); then
                LINE=${COMMIT_CNT}
            fi

            if [[ ${RELEASE_TYPE} == "hotfixed" ]]; then
                git log develop -${LINE} --skip $((LINE-${COMMIT_CNT})) --max-count=${COMMIT_CNT} --grep="(hotfix)"
            else
                git log develop -${LINE} --skip $((LINE-${COMMIT_CNT})) --max-count=${COMMIT_CNT} --grep="(hotfix)" --invert-grep
            fi

            return 0
            ;;
         "3")
            read -p "Give me a hash: " pressed
            if (( ${#pressed} == ${HASH_LEN} )); then
                    COMMIT=${pressed}
                    echo "Use commit ${COMMIT}"
                else
                    echo "Invalid hash"
            fi
            return 0
            ;;
          *)
            return 1
            ;;
    esac
}

# Get the next version: 10.20.102 => 10.20.103
next_ver()
{
    VER=$1
    VER_HEX=$(printf '%.2X%.2X%.2X\n' `echo $VER | sed -e 's/\./ /g'`)
    NEXT_VER_HEX=$(printf %.6X `echo $(( 0x$VER_HEX + 1 ))`)
    NEXT_VER=$(printf '%d.%d.%d\n' `echo $NEXT_VER_HEX | sed -r 's/(..)/0x\1 /g'`)
    echo "$NEXT_VER"
}

check_and_save_step()
{
    printf "\n${NICE} Step 1: Check submodules for staged and modified files ... ${NC}"
    cd ${PROJECT_V50_PATH}/../

    CHECK_STAT=""

    if [[ $(git status -s --ignore-submodules) != "" ]]; then
        printf "${RED}\n Repository unifw_devices is dirty.\n Please, fix all stagged, modified, untracked files${NC}\n"
        CHECK_STAT="bad"
    fi

    git submodule status --recursive > submodules_all_info

    while read  line
    do
        path=$(echo ${line#+*} | awk '{ print $2 }')
        cd ${path}

        if [[ $(git status -s --ignore-submodules) != "" ]]; then
            printf "${RED}\n Repository ${path} is dirty. Please, fix all stagged, modified, untracked files${NC}\n"
            CHECK_STAT="bad"
        fi
        cd - > /dev/null
    done < submodules_all_info

    if [[ ${CHECK_STAT} != "" ]]; then
        rm submodules_all_info
        exit 0
    fi
    printf "Done.\n"
}

print_hotfixed_branch()
{
    IS_FOUND=1

    while read  line
    do
        path=$(echo ${line#+*} | awk '{ print $2 }')
        cd ${path}
        
        echo ${path}":"
        IS_HOTFIX=$(git branch | cut -c 3- | grep -e 'hotfix_base/.*Release-.*')

        if [[ ${IS_HOTFIX} != "" ]]; then
            printf "${IS_HOTFIX}\n"
            IS_FOUND=0
        fi
        echo
        cd - > /dev/null
    done < submodules_all_info

    return $IS_FOUND
}

check_err()
{
    if [[ ! $? -eq "0" ]]; then
        printf "${RED} git $1 error:${NC}\n"
        printf "Restore submodules ...\n"
        if [[ ${1} == "rebase" ]]; then
            git rebase --abort
        fi

        finalize
        exit 1
    fi
}

select_device()
{
    cd ${PROJECT_V50_PATH}/..
    DEV_LISTS=($(git branch | cut -c 3- | grep -v "develop\|master\|detached"))

    echo "Avaliable devices:"
    for ((i=0; i<${#DEV_LISTS[@]}; i=i+1)); do
        echo ${i} - ${DEV_LISTS[i]}
    done
    read -p "Select number: " number
    if [[ ${number} < ${#DEV_LISTS[@]} ]]; then
        echo "Active branch:" ${DEV_LISTS[${number}]}
    else
        echo "Invalid number" ${i}. "Max value:" $((${#DEV_LISTS[@]}-1))
        exit 0
    fi
    git checkout ${DEV_LISTS[${number}]}
    ACTIVE_DEVICE=${DEV_LISTS[${number}]}
}

select_release_type()
{
    cd ${PROJECT_V50_PATH}/..

    echo "Release types:"
    for ((i=0; i<${#REL_TYPES[@]}; i=i+1)); do
        echo ${i} - ${REL_TYPES[i]}
    done
    read -p "Select number: " number
    if [[ ${number} < ${#REL_TYPES[@]} ]]; then
        echo "Active release type:" ${REL_TYPES[${number}]}
    else
        echo "Invalid release type" ${i}. "Max value:" $((${#REL_TYPES[@]}-1))
        exit 0
    fi
    RELEASE_TYPE=${REL_TYPES[${number}]}
}

if [[ ${1} == "release" ]]; then

    cd ${PROJECT_V50_PATH}/..

    # Save unifw_devices state
    echo $(git rev-parse HEAD) "." > submodules_all_info

    printf "\n${NICE} Step 1: Get unifw_devices changes ... ${NC}\n"
    git remote update
    printf "Done.\n"

    printf "\n${NICE} Step 2: Update develop branch(get new RC) ... ${NC}\n"
    git checkout develop
    git pull origin develop
    check_err "pull"
    printf "Done.\n"

    printf "\n${NICE} Step 3: Select device(branch) ... ${NC}\n"
    select_device
    printf "Done.\n"

    printf "\n${NICE} Step 4: Check for the new releases for device $(git rev-parse --abbrev-ref HEAD) on the remote ... ${NC}\n"
    git pull origin $(git rev-parse --abbrev-ref HEAD)
    check_err "pull"
    printf "Done.\n"

    printf "\n${NICE} Step 5: Choose release type ... ${NC}\n"
    select_release_type
    printf "Done.\n"

    printf "\n${NICE} Step 6: Select rc commit ... ${NC}\n"
    while [[ ${COMMIT} == "" ]]; do
        if ! select_rc; then echo "Exit."; exit 0; fi
    done
    printf " Done.\n"

    printf "\n${NICE} Step 7: Modify container file (${TOML_CONFIG_NAME}) ... ${NC}\n"
    if [[ -f ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME} ]]; then

        cp ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME} ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME}.backup
        xdg-open ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME}

        echo "----------"
        if [[ $(diff  ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME}.backup ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME}) ]]; then
            git diff --no-index -- ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME}.backup ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME}
        else
            echo "No changes."
        fi
        echo "----------"

        read -e -n 1 -p "Check your changes. Is it correct? (y/n): " answer
        if [[ ${answer} == "y" ]]; then
            echo "Changes applied."
            rm ${PROJECT_V50_PATH}/../config.toml.backup
        else
            cp ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME}.backup ${PROJECT_V50_PATH}/../${TOML_CONFIG_NAME}
            echo "Original ${TOML_CONFIG_NAME} restored."
            echo "Exit."
            rm -f REL_MESSAGE
            rm -f submodules_all_info
            exit 0
        fi

    else
        printf "\n${RED} No config.toml file in unifw_devices! Some version strings will be empty! ${NC}\n"
    fi

    printf "\n${NICE} Step 8: Create release for ${ACTIVE_DEVICE} ... ${NC}\n"

    # Find tag with the max version. Except hotfixed tags
    MAX_VER_IN_TAG=$(git tag -l *-*-* | grep -vE ".*-.*-.*-.*" | cut -d '-' -f 3  | sort -r | head -1)

    ALL_RC_TAGS=""
    # Calculate next tag version. For hotfixed release based on RC commits info
    for h_tag_or_hash in $(git show -s --format=%b ${COMMIT} | awk '{printf "%s\n",$3 }'); do

        # Check hotfixed Release or not. Example:
        # MNV202-Release-0.0.71-3 and MNV202-Release-0.0.71
        if [[ $(grep -o '-' <<<"$h_tag_or_hash" | grep -c .) != "2" ]]; then
            # Release hotfixed, remove tail
            ALL_RC_TAGS+=${h_tag_or_hash}" "
            CURR_VER=${h_tag_or_hash%-*}
            CURR_VER=${CURR_VER##*-}
        fi
    done

    if [[ ${RELEASE_TYPE} == "hotfixed" ]]; then
        MAX_TAG=$(echo ${ALL_RC_TAGS} | tr ' ' '\n' | sort -r | tr '\n' ' ' | awk '{printf $1}')
        read -e -p "Set release verison. Previous $(git tag -l MNV202-Release-*-* | sort -r -V | head -1) (hotfixed): " -i "${MAX_TAG}" REL_VER
    else
        MAX_VER_IN_TAG=$(git tag | grep -x ".*${MAX_VER_IN_TAG}")
        CURR_VER=${MAX_VER_IN_TAG##*-}
        read -e -p "Set release verison. Previous ${MAX_VER_IN_TAG} (ordinary Release) : " -i "${ACTIVE_DEVICE}-Release-$(next_ver $CURR_VER)" REL_VER
    fi

    REL_MESSAGE="${REL_VER} info:\n\n"
    REL_MESSAGE+="Date: $(date +"%d.%m.%Y %H:%M")\n"
    REL_MESSAGE+="Commit: ${COMMIT}\n\n"
    REL_MESSAGE+="Changes:\n"
    msg=$(git show -s --format=%b ${COMMIT})
    REL_MESSAGE+=$(printf "${msg}")"\n"
    printf "${REL_MESSAGE}" > REL_MESSAGE

    if [[ -f ${TOML_CONFIG_NAME} ]]; then
        git add ${TOML_CONFIG_NAME}
    fi
    git add $(git submodule status | awk '{ print $2 }')
    git commit --allow-empty -F REL_MESSAGE
    git tag ${REL_VER}
    git push origin ${ACTIVE_DEVICE} ${REL_VER}
    check_err "push"
    printf " Done.\n"

    rm -f REL_MESSAGE
    rm -f submodules_all_info

    exit 0

elif [[ ${1} == "sync" ]]; then

    check_and_save_step
    cd ${PROJECT_V50_PATH}/../

    printf "${NICE} Step 2: Get remote changes ...  ${NC}\n"
    git submodule foreach --recursive git remote update
    git remote update
    printf " Done.\n"

    printf "${NICE} Step 3: Try to find hotfix branches ...  ${NC}\n"
    print_hotfixed_branch
    printf " Done.\n"
    HOTFIX=$?
    NEW_TAG=""

    if [[ ${HOTFIX} = "0" ]]; then
        read -e -p "Synchronize hotfix branch (for every submodule) or only master? (master/ select hotfix_base) ?: " -i "master" answer
        HOTFIX=${answer} 

        if [[ ${HOTFIX} == "" ]]; then
            printf "${RED} This field can't be empty! Write \"master\" or \"hotfix_base/*\" branch name! ...${NC}\n"
            finalize
            exit 1
        fi
        CURR_VER=${HOTFIX%/*}
        CURR_VER=${CURR_VER#*/}
    else
        HOTFIX="master"
    fi

    if [[ ${HOTFIX} != "master" ]]; then

        printf "${NICE} Step 4: Checkout to your ${HOTFIX} base branch for every submodule (and update if needed).\n\
        Increment tag ${CURR_VER} for hotfixed releases branch ...  ${NC}\n"
        while read  line
        do
            path=$(echo ${line#+*} | awk '{ print $2 }')
            cd ${path}
            
            if [[ $(git branch | grep hotfix_changes/${CURR_VER#*/}) == "" ]]; then
                printf "${NICE}\n Repository ${path} without changes.\n\
 So I use the original commit ${HOTFIX} from hotfixed_releases ${CURR_VER}. ${NC}\n"
                git checkout ${HOTFIX}
                cd - > /dev/null
                continue;
            fi

            git checkout ${HOTFIX}
            git pull origin ${HOTFIX}
            check_err "pull"

            # Find hotfixed tags: MNV202-Release-0.0.51 -> no hotfix. MNV202-Release-0.0.51-x -> with hotfix
            if [[ $(grep -o '-' <<<"$CURR_VER" | grep -c .) != "2" ]]; then

                LAST_HOTFIXED=$(git tag --sort=-refname | grep ${CURR_VER%-*} | head -1)

                if [[ ${LAST_HOTFIXED} != "" ]]; then
                    printf "${NICE} For this Release the last hotfix is ${LAST_HOTFIXED}.\nUpdate it to ${NEW_TAG} ${NC}\n"
                else
                    printf "${NICE} It's first fix for this submodule $(basename ${PWD}), but other already have changes.\n Use version from them ${NC}\n"
                fi

                COUNTER=$(echo ${CURR_VER##*-})
                ((COUNTER++))
                NEW_TAG=$(echo ${CURR_VER%-*})-${COUNTER}
                git tag -f ${NEW_TAG}
                git push origin ${NEW_TAG}
                check_err "push"

            else
                printf "${NICE} It's first fix for this Release: ${CURR_VER}. Create new tag ${CURR_VER}-0 ${NC}\n"
                git tag -f ${CURR_VER}-0
                git push origin ${CURR_VER}-0
            fi

            cd - > /dev/null
        done < submodules_all_info

        printf " Done.\n"
        printf "${NICE} Steps 5 skipped for hotfix ...  ${NC}\n"

    else
        printf "${NICE} Step 4: For every submodule go to master(unifw_devices to develop) ...  ${NC}\n"
        git checkout develop
        git submodule foreach --recursive git checkout master -q
        printf " Done.\n\n"

        printf "${NICE} Step 5: Synchronize remote changes with the local ...  ${NC}\n"
        git submodule foreach --recursive git pull -q
        check_err "pull"
        printf " Done.\n\n"
    fi

    printf "${NICE} Step 6: Combine submodules to one commit in ${PROJECT_V50_PATH} ...  ${NC}\n"
    cd ${PROJECT_V50_PATH}
    git submodule status > submodules_info
    # Unite submodules in the "unifw_icatch_v50 level" to one commit
    SYNC_MESSAGE="Sync submodules: \n\n"

    msg=$(git log --format="%h %s" -n 1)
    if [[ ${HOTFIX} != "master" ]]; then

        CURRENT_TAG=$(git tag --points-at HEAD)
        if [[ ${CURRENT_TAG} != "" ]]; then
            msg="${CURRENT_TAG} $(git log --format="%s" -n 1)"
        fi
    fi
    repo=$(basename $(git rev-parse --show-toplevel))
    SYNC_MESSAGE+=$(printf "%-18s -> ${msg}" "${repo}")"\n"

    while read  line
    do
        path=$(echo ${line#+*} | awk '{ print $2 }')
        cd ${path}

        CURRENT_TAG=$(git tag --points-at HEAD)
        if [[ ${CURRENT_TAG} != "" ]]; then
            msg="${CURRENT_TAG} $(git log --format="%s" -n 1)"
        else
            # Format example: unifw_devices  fd8b5ef  Sync all with feature/RDUF-815-key-gen
            msg=$(git log --format="%h %s" -n 1)
        fi

        repo=$(basename $(git rev-parse --show-toplevel))
        SYNC_MESSAGE+=$(printf "%-18s -> ${msg}" "${repo}")"\n"

        cd - > /dev/null
    done < submodules_info
    rm submodules_info
    printf "${SYNC_MESSAGE}" > SYNC_MESSAGE

    git add $(git submodule status | awk '{ print $2 }') > /dev/null
    git commit --allow-empty -F SYNC_MESSAGE 
    rm SYNC_MESSAGE

    PUSH_RESULT=""
    if [[ ${HOTFIX} == "master" ]]; then
        git push origin master -q
        PUSH_RESULT=$?
    else
        git pull origin ${HOTFIX} -q
        check_err "pull"
        git push origin ${HOTFIX} -q
        PUSH_RESULT=$?
    fi

    if [[ ! ${PUSH_RESULT} -eq "0" ]]; then
        printf "${RED} git push error. Restore your repo ...${NC}\n"
        finalize
        exit 1
    fi

    cd ${PROJECT_V50_PATH}/../
    printf " Done.\n\n"

    printf "${NICE} Step 7: Prepare rc commit message ...  ${NC}"

    if [[ ${HOTFIX} != "master" ]]; then
        RC_MESSAGE="Build( hotfix): $(git rev-list --count HEAD --remotes)\n\n"
    else
        RC_MESSAGE="Build: $(git rev-list --count HEAD --remotes)\n\n"
    fi
    git submodule status > submodules_info

    cd ${PROJECT_V50_PATH}
    msg=$(git log --format="%b" -n 1)
    RC_MESSAGE+=$(printf "${msg}")"\n"
    cd ${PROJECT_V50_PATH}/../

    while read  line
    do
        path=$(echo ${line#+*} | awk '{ print $2 }')
        # we have already grubbeb this info at the few lines above
        if [[ ${path} == "unifw_icatch_v50" ]]; then
            continue;
        fi

        cd ${path}

        CURRENT_TAG=$(git tag --points-at HEAD)
        if [[ ${CURRENT_TAG} != "" ]]; then
            msg="${CURRENT_TAG} $(git log --format="%s" -n 1)"
        else
            # Format example: unifw_devices  fd8b5ef  Sync all with feature/RDUF-815-key-gen
            msg=$(git log --format="%h %s" -n 1)
        fi
        repo=$(basename $(git rev-parse --show-toplevel))
        RC_MESSAGE+=$(printf "%-18s -> ${msg}" "${repo}")"\n"

        cd - > /dev/null
    done < submodules_info

    printf "${RC_MESSAGE}" > RC_MESSAGE
    printf "Done.\n"

    printf "${NICE} Step 8: Get develop branch in ${PROJECT_V50_PATH}/.. repo ...  ${NC}\n"
    git remote update

    if [[ $(git branch | grep develop) == "" ]]; then
        git checkout -b develop -q
    else
        git checkout develop
    fi
    git pull origin develop
    check_err "pull"
    printf " Done.\n\n"

    printf "${NICE} Step 9: Create release candidate commit ...  ${NC}\n"
    git add $(git submodule status | awk '{ print $2 }')
    git add unifw_icatch_v50
    git commit -F RC_MESSAGE
    rm RC_MESSAGE

    git push origin develop -q
    check_err "push"
    printf " Done.\n"

elif [[ ${1} == "hotfix" ]]; then

    if [[ ${2} == "begin" ]]; then

        check_and_save_step

        printf "\n${NICE} Step 2: Get unifw_devices changes ... ${NC}\n"
        cd ${PROJECT_V50_PATH}/../
        git remote update
        printf "Done.\n"

        printf "${NICE} Step 3: Select device(branch) ... ${NC}\n"
        cd ${PROJECT_V50_PATH}/../
        select_device
        printf "Done.\n"

        printf "\n${NICE} Step 4: Update device branch ${ACTIVE_DEVICE} ... ${NC}\n"
        git checkout ${ACTIVE_DEVICE}
        git pull origin ${ACTIVE_DEVICE}
        check_err "pull"
        printf "Done.\n"

        printf "\n${NICE} Step 5: Select release tag ... ${NC}\n"
        PREV_TAG=$(git describe --abbrev=0 --tags)
        read -e -p $'Enter the tag name: ' -i "${PREV_TAG}" TAG

        if git rev-parse "$TAG" >/dev/null 2>&1; then

            ACTIVE_DEVICE=$(git rev-parse --abbrev-ref HEAD)
            git checkout ${TAG} -q
            printf "Switched to ${NICE} ${TAG} ${NC}\n"

            printf "\n${NICE} Step 6: Restore submodules tree for the  ${TAG} ... ${NC}\n"
            repo_state_list=$(git show -s --format=%b HEAD | tail -n +5)

            while read  line
            do
                path=$(echo ${line#+*} | awk '{ print $2 }')
                cd ${path}

                repo_name=${path#*/}

                commit=$(printf "${repo_state_list}\n" | grep -E "${repo_name}.*->" | awk '{printf $3}')
                spaces_30=$(printf '%-30s')
                printf "In ${path} go to %s %s ${NICE} ${commit} ${NC}\n" "${spaces_30:${#path}}"

                git checkout ${commit} -q
                check_err "checkout"

                cd - > /dev/null
            done < submodules_all_info
        else
          echo "Invalid tag name"
          exit 0
        fi
        printf " Done.\n"

        printf "\n${NICE} Step 7: Specify task name for this hotfix. Format: TASK-ID-short-descr ... ${NC}\n"
        cd ${PROJECT_V50_PATH}/../
        read -p "Task name: " TASK_NAME
        if [[ ${TASK_NAME} == "" ]]; then
            echo "Invalid task name"
            finalize
            exit 0
        fi
        printf "Done.\n"

        printf "\n${NICE} Step 8: Create new branch(based on ${TAG}) for every submodule ...  ${NC}\n"
        REL_VER=${TAG}
        BRANCH_NAME="hotfix_base/"${REL_VER}/${TASK_NAME}
        read -e -n 1 -p "Branch name ${BRANCH_NAME}. Is it correct? (y/n): " answer
        echo
        if [[ ${answer} == "n" ]]; then
            echo "Exit."
            finalize
            exit 0
        elif [[ ${answer} == "y" ]]; then

            while read  line
            do
                path=$(echo ${line#+*} | awk '{ print $2 }')
                cd ${path}
                git checkout -B ${BRANCH_NAME}
                git fetch origin ${BRANCH_NAME} &> /dev/null
                if [[ ! $? -eq "0" ]]; then
                    echo "Remote don't have this branch. Push it"
                    git push origin ${BRANCH_NAME}
                else
                    echo "Remote for ${path} already contains branch ${BRANCH_NAME}. Update our local ..."
                    git rebase "remotes/origin/"${BRANCH_NAME}
                    check_err "rebase"
                fi

                cd - > /dev/null
            done < submodules_all_info
            rm -f submodules_all_info
        else
            echo "Exit."
            finalize
            exit 0
        fi
        printf " Done.\n"

        printf "${NICE}\n Now you must create yout hofix branch where you want. Use the template below:${NC}\n"
        printf "${NICE} git checkout -b hotfix_changes/${REL_VER}/${TASK_NAME} ... ${NC}\n\n"

        exit 0

    elif [[ ${2} == "end" ]]; then

        check_and_save_step

        printf "${NICE} Step 2: Select device(branch) ... ${NC}\n"
        cd ${PROJECT_V50_PATH}/../
        select_device
        printf "Done.\n"

        printf "${NICE} Step 3: Push all hotfix branches ...  ${NC}\n"
        cd ${PROJECT_V50_PATH}/../
        HOTFIX_BRANCH=""
        BASE_BRANCH=""
        while read  line
        do
            path=$(echo ${line#+*} | awk '{ print $2 }')
            cd ${path}

            if [[ ${HOTFIX_BRANCH} != "" ]];then
                REPO_HOT_BRANCH=${HOTFIX_BRANCH}

                if [[ $(git branch | grep ${REPO_HOT_BRANCH}) == "" ]]; then
                    printf "${NICE} No ${REPO_HOT_BRANCH} branch. {NC}\n"
                    git checkout ${REPO_BASE_BRANCH}
                    echo "Repository skipped."
                    cd - > /dev/null
                    continue;
                fi

            else
                # branch with our changes
                REPO_HOT_BRANCH=$(git branch | cut -c 3- | grep -e 'hotfix_changes/.*-Release-.*')

                if (( $(printf "${REPO_HOT_BRANCH}\n" | wc -l) > 1 )); then
                    printf "${NICE} Found more then 1 hotfix branch in ${path}:  ${NC}\n"
                    printf "${REPO_HOT_BRANCH}\n"
                    read -p "Select your branch (or Enter for skip this repo): " h_branch </dev/tty

                    if [[ ${REPO_HOT_BRANCH} != *"${h_branch}"* ]]; then
                        printf "${RED} Invalid hotfix branch ${h_branch} !${NC}\n"
                        finalize
                        exit 1
                    elif [[ ${h_branch} == "" ]]; then
                        echo "Repository skipped."
                        cd - > /dev/null
                        continue;
                    else
                        REPO_HOT_BRANCH=${h_branch}

                        if [[ ${HOTFIX_BRANCH} == "" ]];then
                            HOTFIX_BRANCH=${REPO_HOT_BRANCH}
                        fi

                        REPO_BASE_BRANCH=hotfix_base/${REPO_HOT_BRANCH#*/}

                        if [[ $(git branch | grep ${REPO_BASE_BRANCH}) != "" ]]; then

                            if [[ ${BASE_BRANCH} == "" ]];then
                                BASE_BRANCH=${REPO_BASE_BRANCH}
                            fi

                            if [[ $(git ls-remote --heads | grep ${REPO_BASE_BRANCH}) != "" ]]; then
                                git pull origin ${REPO_BASE_BRANCH}
                                check_err "pull"
                            fi

                            git push origin ${REPO_BASE_BRANCH}
                            check_err "push"
                        else
                            printf "${RED} Weird repo. Missing base branch for ${REPO_HOT_BRANCH} ${NC}\n"
                            finalize
                            exit 1
                            break;
                        fi
                    fi
                fi
            fi

            if [[ $(git diff ${REPO_BASE_BRANCH} ${REPO_HOT_BRANCH}) != "" ]]; then

                if [[ $(git ls-remote --heads | grep ${REPO_HOT_BRANCH}) != "" ]]; then
                    git pull origin ${REPO_HOT_BRANCH}
                    check_err "pull"
                fi

                git push origin ${REPO_HOT_BRANCH}
                check_err "push"
            else
                printf "${NICE} base branch ${REPO_BASE_BRANCH} and ${REPO_HOT_BRANCH} the same.\nDelete ${REPO_HOT_BRANCH} and go to base${NC}\n"
                git checkout ${REPO_BASE_BRANCH}
                git branch -D ${REPO_HOT_BRANCH}  &> /dev/null
            fi

            cd - > /dev/null
        done < submodules_all_info

        rm -f submodules_all_info

        cd - > /dev/null

        if [[ ${HOTFIX_BRANCH} == "" ]]; then
            printf "\n ${RED} No hotfix branch with changes in any submodule!\n ${NC}\n"
        else
            printf "\n Now go to server and create pull request from\n"
            printf " ${NICE}${HOTFIX_BRANCH} ${NC}\nto\n"
            printf " ${NICE}${BASE_BRANCH} ${NC}\n\n"
            printf " ${NICE}When your changes will have merged, call 'make sync' and choose hotfixed branch ${NC}\n"
        fi

        printf "Done.\n"
        exit 0

    else
        echo "Invalid hotfix argument!"
        exit 0
    fi

elif [[ ${1} == "clean" ]]; then
    printf "${NICE} Clean unused branches. ${NC}\n"

    cd ${PROJECT_V50_PATH}/../
    git submodule status --recursive > submodules_all_info

    while read  line
    do
        path=$(echo ${line#+*} | awk '{ print $2 }')
        cd ${path}

        git branch | grep hotfix_base/ | xargs git branch -D
        git branch | grep hotfix_changes/ | xargs git branch -D
        git fetch -p origin
        for i in $(git branch -a | grep remotes/origin/hotfix | cut -d'/' -f 3-); do git push origin -d ${i}; done
        for i in $(git branch -a | grep remotes/origin/base | cut -d'/' -f 3-); do git push origin -d ${i}; done

        cd - > /dev/null
    done < submodules_all_info
    rm -f submodules_all_info

else
    help
    exit 0
fi

echo
finalize
